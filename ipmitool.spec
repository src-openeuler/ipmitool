Name:            ipmitool
Version:         1.8.18
Release:         23
Summary:         Utility for IPMI control
License:         BSD
URL:             https://codeberg.org/IPMITool/ipmitool
Source0:         http://downloads.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.bz2
Source1:         ipmievd.sysconf
Source2:         ipmievd.service
Source5:         set-bmc-url.sh

Patch1:          0001-CVE-2011-4339-OpenIPMI.patch
Patch2:          0002-openssl.patch
Patch3:          0003-ipmitool-1.8.11-set-kg-key.patch
Patch4:          0004-slowswid.patch
Patch5:          0005-zero-initialize-the-recv-structure-on-the-stack.patch

Patch6000:       ID-477-fru-Fix-decoding-of-non-text-data-in-get_fru_.patch
Patch6001:       ID-480-ipmitool-coredumps-in-EVP_CIPHER_CTX_init.patch
Patch6002:       ID-480-Call-EVP_CIPHER_CTX_free-instead-of-EVP_CIPHE.patch
Patch6003:       ID-472-Fix-The-Most-recent-Addition-Erase-date.patch
Patch6004:       ID-508-Fix-segfaults-in-dcmi-command-handlers.patch
Patch6005:       ID-508-Refix-6d9c540-Forgotten-changes.patch
Patch6006:       Re-apply-commit-58d510f90feb.patch
Patch6007:       fru-internaluse-Fix-segmentation-fault-9.patch
Patch6008:       Replace-user_id-masks-with-a-macro-8.patch
Patch6009:       plugins-open-Fix-for-interrupted-select.patch
Patch6010:       plugins-open-Properly-enable-event-receiver-35.patch
Patch6011:       lanplus-Cleanup.-Refix-6dec83ff-fix-be2c0c4b.patch
Patch6012:       lanplus-Fix-segfault-for-truncated-dcmi-response.patch
Patch6013:       helper-add-free_n-method-to-handle-clearing-pointers.patch
Patch6014:       sol-Make-interface-timeout-obey-the-N-option.patch
Patch6015:       hpm-Fix-resource-leak.patch
Patch6016:       fru-fixup-array-bounds-checking.patch
Patch6017:       fru-swap-free-calls-for-free_n.patch
Patch6018:       Refactor-free_n-function.patch
Patch6019:       open-checking-received-msg-id-against-expectation.patch

Patch6020:       ipmitool-CVE-2020-5208-Fix-buffer-overflow-vulnerabilities.patch
Patch6021:       ipmitool-CVE-2020-5208-Fix-buffer-overflow-in-ipmi_spd_print_fru.patch
Patch6022:       ipmitool-CVE-2020-5208-Fix-buffer-overflow-in-ipmi_get_session_info.patch
Patch6023:       ipmitool-CVE-2020-5208-Fix-buffer-overflow.patch
Patch6024:       ipmitool-CVE-2020-5208-Fix-buffer-overflows-in-get_lan_param_select.patch
Patch6025:       ipmitool-CVE-2020-5208-Fix-id_string-buffer-overflows.patch
Patch6026:       fix-variable-definition-error-with-gcc-10.patch
Patch6027:       backport-lanplus-Realloc-the-msg-if-the-payload_length-gets-u.patch
Patch6028:       backport-lan-channel-Fix-set-alert-on-off.patch
Patch6029:       backport-fru-Fix-crashes-on-6-bit-ASCII-strings.patch

Patch9000:       0006-Fix-get_bmc_ip-failure-caused-by-search-rule-issue.patch

BuildRequires:    openssl-devel readline-devel ncurses-devel 
%{?systemd_requires}
BuildRequires:   systemd
BuildRequires:   automake autoconf libtool
Obsoletes:       OpenIPMI-tools < 2.0.14-3
Provides:        OpenIPMI-tools = 2.0.14-3
Obsoletes:       ipmievd
Provides:        ipmievd

%description
This package provides a simple command-line interface to IPMI-enabled devices 
through an IPMIv1.5 or IPMIv2.0 LAN interface or Linux/Solaris kernel driver.

%package -n bmc-snmp-proxy
Requires: net-snmp
Requires: exchange-bmc-os-info
BuildArch: noarch
Summary: Reconfigure SNMP to include host SNMP agent within BMC

%description -n bmc-snmp-proxy
Given a host with BMC, this package would extend system configuration
of net-snmp to include redirections to BMC based SNMP.


%package -n exchange-bmc-os-info
Requires: hostname
Requires: ipmitool
BuildArch: noarch
%{?systemd_requires}
BuildRequires: systemd
Summary: Let OS and BMC exchange info

%description -n exchange-bmc-os-info
Given a host with BMC, this package would pass the hostname &
OS information to the BMC and also capture the BMC ip info
for the host OS to use.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

for f in AUTHORS ChangeLog; do
    iconv -f iso-8859-1 -t utf8 < ${f} > ${f}.utf8
    mv ${f}.utf8 ${f}
done

%build
aclocal
libtoolize --automake --copy
autoheader
automake --foreign --add-missing --copy
aclocal
autoconf
automake --foreign

%configure --disable-dependency-tracking --enable-file-security --disable-intf-free
%make_build

%check
make check

%install
%make_install

install -Dpm 644 %{SOURCE2} %{buildroot}%{_unitdir}/ipmievd.service
install -Dpm 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/sysconfig/ipmievd

install -Dm 644 contrib/exchange-bmc-os-info.service.redhat %{buildroot}%{_unitdir}/exchange-bmc-os-info.service
install -Dm 644 contrib/exchange-bmc-os-info.sysconf        %{buildroot}%{_sysconfdir}/sysconfig/exchange-bmc-os-info
install -Dm 755 contrib/exchange-bmc-os-info.init.redhat    %{buildroot}%{_libexecdir}/exchange-bmc-os-info
install -Dm 644 %{SOURCE5} %{buildroot}%{_sysconfdir}/profile.d/set-bmc-url.sh

install -Dm 644 contrib/bmc-snmp-proxy.sysconf %{buildroot}%{_sysconfdir}/sysconfig/bmc-snmp-proxy
install -Dm 644 contrib/bmc-snmp-proxy.service %{buildroot}%{_unitdir}/bmc-snmp-proxy.service
install -Dm 755 contrib/bmc-snmp-proxy         %{buildroot}%{_libexecdir}/bmc-snmp-proxy

%post
%systemd_post ipmievd.service

%preun
%systemd_preun ipmievd.service

%postun
%systemd_postun_with_restart ipmievd.service

%post -n exchange-bmc-os-info
%systemd_post exchange-bmc-os-info.service

%preun -n exchange-bmc-os-info
%systemd_preun exchange-bmc-os-info.service

%postun -n exchange-bmc-os-info
%systemd_postun_with_restart exchange-bmc-os-info.service

%triggerun -- %{name}
/usr/bin/systemd-sysv-convert --save ipmievd >/dev/null 2>&1 ||:
/sbin/chkconfig --del ipmievd >/dev/null 2>&1 || :
/bin/systemctl try-restart ipmievd.service >/dev/null 2>&1 || :

%files
%doc %{_datadir}/doc/ipmitool/AUTHORS
%doc %{_datadir}/doc/ipmitool/COPYING
%config(noreplace) %{_sysconfdir}/sysconfig/ipmievd
%{_bindir}/ipmitool
%{_sbindir}/ipmievd
%{_unitdir}/ipmievd.service
%{_datadir}/ipmitool

%files -n exchange-bmc-os-info
%config(noreplace) %{_sysconfdir}/sysconfig/exchange-bmc-os-info
%{_sysconfdir}/profile.d/set-bmc-url.sh
%{_unitdir}/exchange-bmc-os-info.service
%{_libexecdir}/exchange-bmc-os-info

%files -n bmc-snmp-proxy
%config(noreplace) %{_sysconfdir}/sysconfig/bmc-snmp-proxy
%{_unitdir}/bmc-snmp-proxy.service
%{_libexecdir}/bmc-snmp-proxy

%files help
%doc %{_datadir}/doc/ipmitool/README
%doc %{_datadir}/doc/ipmitool/ChangeLog
%{_mandir}/man1/ipmitool.1*
%{_mandir}/man8/ipmievd.8*

%changelog
* Tue Oct 08 2024 zhangzikang <zhangzikang@kylinos.cn> - 1.8.18-23
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix get_bmc_ip failure caused by search rule issue

* Mon Dec 04 2023 Huang Yang <huangyang@loongson.cn> - 1.8.18-22
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Resolve malloc aborted error raised when executing ipmitool fru.

* Wed May 31 2023 mengkanglai <mengkanglai2@huawei.com> - 1.8.18-21
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update URL address

* Tue Mar 21 2023 mengkanglai <mengkanglai2@huawei.com> - 1.8.18-20
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:lan channel fix set alert on off and lanplus realloc the msg if the payload_length gets update

* Mon Oct 31 2022 mengkanglai <mengkanglai2@huawei.com> - 1.8.18-19
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:zero initialize the recv structure on the stack

* Mon Dec 27 2021 gaihuiying <gaihuiying1@huawei.com> - 1.8.18-18
- Type:requirement
- ID:NA
- SUG:NA
- DESC:separate exchange-bmc-os-info,bmc-snmp-proxy from ipmitool

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.8.18-17
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Fri Jul 30 2021 gaihuiying <gaihuiying1@huawei.com> - 1.8.18-16
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix build error with gcc 10

* Tue Dec 15 2020 xihaochen <xihaochen@huawei.com> - 1.8.18-15
- Type:requirement
- Id:NA
- SUG:NA
- DESC:remove sensitive words 

* Fri Mar 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.8.18-14
- enable make check

* Fri Mar 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.8.18-13
- fix CVE-2020-5208

* Mon Jan 6 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.8.18-12
- update software package

* Sat Sep 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.8.18-11
- Package init
